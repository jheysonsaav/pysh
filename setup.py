from setuptools import setup, find_packages

setup(
    name="pysh",
    version="0.1.0",
    author="JheysonDev",
    url="https://gitlab.com/JheysonDev/pysh",
    license="GNU Public License",
    packages=find_packages(),
)
